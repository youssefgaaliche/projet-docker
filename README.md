# JuniorCV is a Docker based MEAN Stack
##### JuniorCV  is a cv designing platform developped in the soul purpose is helping Student take their first step into the professional life 

### 1.Installation

Please install [Docker](https://docs.docker.com/install/#supported-platforms) and  [Docker Compose](https://docs.docker.com/compose/install/)


```sh
$ cd mean
```

##### Angular Client
```sh
$ cd angular-client 
$ npm install
```

##### Express Server
```sh
$ cd express-server 
$ npm install

```


##### Start Command
```sh
$ cd mean
$ docker-compose up -d
```

### 2.Test
##### Angular Client
[http://localhost:4200](http://localhost:4200)

##### Express Server
[http://localhost:3000](http://localhost:3000)

# Author

Youssef Gaaliche (IA2.2) [GitLab](https://gitlab.com/youssefgaaliche?nav_source=navbar)



